#define EA1 11
#define EA2 10
#define IN1 5
#define IN2 4
#define IN3 3
#define IN4 2
#define FRONTSWITCH 7
#define REARSWITCH 6
#define SPEEDADJUST 0

int raw_pot_value = 0;
int pwm = 0;

void setup() {
  Serial.begin(9600);
  // put your setup code here, to run once:
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  pinMode(FRONTSWITCH,INPUT);
  pinMode(REARSWITCH,INPUT);
  
  analogWrite(EA1,255);
  digitalWrite(IN1,0);
  digitalWrite(IN2,1);
}

void loop() {
  raw_pot_value = analogRead(SPEEDADJUST);
  pwm = (900-raw_pot_value)*(255.0/900.0);
  Serial.print(raw_pot_value);
  Serial.print(" ");
  Serial.println(pwm);
  analogWrite(EA2,pwm);
  while(digitalRead(6) != LOW){
      moveReverse();
  }
  stopMotor();
  turnOFF();
  delay(10000);
  while(digitalRead(7) != HIGH){
    moveForward();
  }
  stopMotor();
  turnOFF();
  delay(1000);
}

void moveForward(){
  digitalWrite(IN3,0);
  digitalWrite(IN4,1);
}

void moveReverse(){
  digitalWrite(IN3,1);
  digitalWrite(IN4,0);
}

void stopMotor(){
  digitalWrite(IN3,1);
  digitalWrite(IN4,1);
}

void turnOFF(){
  digitalWrite(IN3,0);
  digitalWrite(IN4,0);
}
